package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JunitService {
	
	@Autowired 
	ComUsersRepository cu;
	 
	public ComUser getUser(int id) {
		Optional<ComUser> re=cu.findById(id);
		if(re.isPresent())
			return re.get();
		return null;
	}
	
	public ComUser saveUser(ComUser user) {
		user=cu.save(user);
		return user;
	}
	
	public List<ComUser> getAllUser() {
		List<ComUser> re=cu.findAll();
		return re;
	}
}

