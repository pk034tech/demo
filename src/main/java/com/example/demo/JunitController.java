package com.example.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class JunitController {
    
	@Autowired
	JunitService jservice;
	
	private static final List<ComUser> users =Arrays.asList(new ComUser("pk"),
			new ComUser("hari"),new ComUser("karthick"));
	
	
	@GetMapping("/user/{id}")
	public ResponseEntity<Object> getUser(@PathVariable int id) {
		//List<ComUser> user=null;
		//user=users.stream().filter(e->e.getUserId()==id).collect(Collectors.toList());
		return ResponseEntity.ok(jservice.getUser(id));
	}
	
	@GetMapping("/user")
	public ResponseEntity<Object> getAllUser() {
		return ResponseEntity.ok(jservice.getAllUser());
	}
	
	@PostMapping("/user")
	public ResponseEntity<Object> addUser(@RequestBody ComUser user) {
		return ResponseEntity.ok(jservice.saveUser(user));
	}
	
	@PutMapping("/user/{id}")
	public ResponseEntity<Object> updateUser(@PathVariable int id,@RequestBody ComUser user) {
		return ResponseEntity.ok(id);
	}
	
	@DeleteMapping("/user/{id}")
	public ResponseEntity<Object> deleteUser(@PathVariable int id) {
		return ResponseEntity.ok(id);
	}
}
