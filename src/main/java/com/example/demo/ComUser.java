package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "com_users")
public class ComUser {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private int userId;

	@Column(name = "name")
	private String name;

	public ComUser() {
		super();
	}
	public ComUser(String name) {
		super();
		this.name = name;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ComUser [userId=" + userId + ", name=" + name + "]";
	}
}
 
  
/*
 * public class ComUser {
 * 
 * private int userId;
 * 
 * private String name;
 * 
 * public ComUser(int userId, String name) { super(); this.userId = userId;
 * this.name = name; }
 * 
 * public int getUserId() { return userId; }
 * 
 * public void setUserId(int userId) { this.userId = userId; }
 * 
 * public String getName() { return name; }
 * 
 * public void setName(String name) { this.name = name; }
 * 
 * @Override public String toString() { return "ComUser [userId=" + userId +
 * ", name=" + name + "]"; }
 * 
 * @Override public int hashCode() { final int prime = 31; int result = 1;
 * result = prime * result + ((name == null) ? 0 : name.hashCode()); result =
 * prime * result + userId; return result; }
 * 
 * @Override public boolean equals(Object obj) { if (this == obj) return true;
 * if (obj == null) return false; if (getClass() != obj.getClass()) return
 * false; ComUser other = (ComUser) obj; if (name == null) { if (other.name !=
 * null) return false; } else if (!name.equals(other.name)) return false; if
 * (userId != other.userId) return false; return true; } }
 */