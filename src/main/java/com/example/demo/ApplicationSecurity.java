
package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class ApplicationSecurity extends WebSecurityConfigurerAdapter {

	private PasswordEncoder passwordEncoder = new PasswordConfig().passwordEncoder();

	@Override 
	protected void configure(HttpSecurity http) throws Exception {
		 http
		 .headers().frameOptions().disable();
		 
		 http 
		.csrf().disable() 
		.authorizeRequests()
		.antMatchers("/testdb/**").permitAll()
		.antMatchers(HttpMethod.POST,"/api/user/**").hasRole("HR")
		.antMatchers(HttpMethod.DELETE,"/api/user/**").hasRole("HR") 
		.antMatchers("/**").permitAll()
		.anyRequest()
		.authenticated().and().httpBasic();
	}

	@Override
	@Bean
	protected UserDetailsService userDetailsService() {
		UserDetails pk = User.builder().username("pk").password(passwordEncoder.encode("password")).roles("HR").build();
		UserDetails hari = User.builder().username("hari").password(passwordEncoder.encode("password")).roles("EMP")
				.build();
		return new InMemoryUserDetailsManager(pk, hari);
	}
}
